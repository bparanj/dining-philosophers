START FROM KERNEL SERVICES FROM THIS LINK
http://www.math.iitb.ac.in/resources/manuals/Unix_Unleashed/Vol_1/ch19.htm


What is a user/kernel interface?
----------------------


----------------------
Diagrams for all concepts.
----------------------


----------------------
Take notes from here:
----------------------
http://meenakshi02.wordpress.com/2011/02/02/strace-hanging-at-futex/
----------------------

Structure of the article
-------------

How to practice strace?
------------------------

```sh
$strace command
```

This prints the details for each system call made by the command.

```sh
$ strace ls
execve("/bin/ls", ["ls"], [/* 35 vars */]) = 0
brk(0)                                  = 0x75f000
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
```

This will show ton of output. Unless you know the system calls, it is difficult to interpret the output.

```sh
$ ps
  PID TTY          TIME CMD
 1890 pts/1    00:00:00 bash
 8809 pts/1    00:00:00 ps
```

Here I have listed two processes running on my machine.

```sh
$ strace -p 1890
attach: ptrace(PTRACE_ATTACH, ...): Operation not permitted
Could not attach to process.  If your uid matches the uid of the target
process, check the setting of /proc/sys/kernel/yama/ptrace_scope, or try
again as the root user.  For more details, see /etc/sysctl.d/10-ptrace.conf
```

This requires sudo access. Let's run it as sudo:

```sh
$ sudo strace -p 1890
[sudo] password for bparanj:
Process 1890 attached - interrupt to quit
wait4(-1,
```

There will be no other output. It just waits.

```sh
~$ sudo strace -p 1386
Process 1386 attached - interrupt to quit
futex(0x2217bc8, FUTEX_WAIT, 0, NULL
```

Here the process with id 1386 is the Virtual Box Client that I am running on my Mac.
It is in a wait state.

What is the most frequently used system calls?
------------------------

Here is the 12 key system calls that you'll see regularly in strace output:

read	  - read bytes from a file descriptor (file, socket)
write   - write bytes from a file descriptor (file, socket)
open	  - open a file (returns a file descriptor)
close   - close a file descriptor
fork    - create a new process (current process is forked)
exec    - execute a new program
connect - connect to a network host
accept  - accept a network connection
stat    - read file statistics
ioctl   - set I/O properties, or other miscellaneous functions
mmap    - map a file to the process memory address space
brk     - extend the heap pointer

Unix Concepts
=============
-------------

What is a file descriptor?
----------------------



What is a system call?
----------------------

A system call is a request for the operating system to do something on behalf
of the user's program. The system calls are functions used in the kernel.
To the programmer, the system call appears as a normal C function call.

UNIX system calls are used to manage the file system, control processes,
and to provide interprocess communication.  The UNIX system interface
consists of about 80 system calls

When a system call encounters an error, it returns -1 and stores the
reason for failure in an external variable named "errno".  The
"/usr/include/errno.h" file maps these error numbers to manifest
constants.

When a system call returns successfully, it returns something other than
-1, but it does not clear "errno".  "errno" only has meaning directly
after a system call that returns an error.

What is a signal?
----------------------

Signals represent a very limited form of interprocess communication.
Signals are sent explicitly to a process from another process using the kill function.
Alternatively, signals are sent to a process from the hardware (to indicate things like
illegal operator, or illegal address) through mediation of the OS.

There are three actions that can take place when a signal is delivered to a process:

1. It can be ignored
2. The process can be terminated
3. A handler function can be called.

What is a Kernel?
----------------------

The UNIX Kernel is the core of the operating system. It provides the basic software abstraction to the hardware.
Regardless of the hardware, the connection between the user and the kernel is generic. It manages the user
program's access to the hardware and software resources. These resources can be scheduling time in the CPU for
the program to run, allocating memory to user's program, providing access to input/output devices such as reading
and writing to the disk drives, connecting to the network and interacting with the terminal or GUI interface.

The basic UNIX kernel provides four services:

Process Management
Memory Management
I/O Management
File Management

These are separate entities that work together to provide services to a program that does meaningful work.
A user can access a database via a Web application, print a report etc. Many programs can request services
at the same time. The kernel schedules work, checks if the process is authorized and grants access to
these services. Programs use software libraries and the systems call interface to utilize these services.

Strace
========
--------

What is strace?
----------------

According to the man page: strace - trace system calls and signals

Why is this useful?
----------------------

Since system calls and signals are events that happen at the user/kernel interface, a close examination
of this boundary is very useful for bug isolation, sanity checking and attempting to capture race conditions.

Examples
=========
---------

1. Find out which config files a program reads on startup
----------------------

```sh
$ strace -e open php 2>&1 | grep php.ini
open("/usr/local/bin/php.ini", O_RDONLY) = -1 ENOENT (No such file or directory)
open("/usr/local/lib/php.ini", O_RDONLY) = 4
```

Have multiple versions of a library installed at different paths and wonder exactly which actually gets loaded?
Use strace to find out. Errors (typically a return value of -1) have the errno symbol and error string appended.
In this case you can see the first line in the output is the error.

2. Why does this program not open my file?
----------------------

How do you find out if a program silently refuses to read a file it doesn't have read access to?

```sh
$ strace -e open,access 2>&1 | grep your-filename
```

Look for an open() or access() syscall that fails

3. What is that process doing right now?
----------------------

Find pid of a process hogging lots of CPU or a process that is hanging:

```sh
root@dev:~# strace -p 15427
Process 15427 attached - interrupt to quit
futex(0x402f4900, FUTEX_WAIT, 2, NULL
Process 15427 detached
```

A futex is a locking mechanism in the Linux kernel. This process is hanging in a call to futex().
This can be caused by a lot of things. But "strace -p" is highly useful because it
removes a lot of guesswork, and often eliminates the need for restarting an app with more
extensive logging.

```sh
strace -p $pid
read(4,...)
```

$pid is the process id of the process to monitor. The PID can be obtained from top or ps.
Use lsof to see if the '4' is a connection to the database.
If the process is just reading from the database handle, then it's probably waiting for a lock.

[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]

4. What is taking time?
----------------------

It is useful to attach strace to a process to see what it's currently spending time on, to diagnose problems.
It is actually doing real work or is it spinning out of control?

Start strace with -c -p, wait for sometime and then exit with ctrl-c. Strace will spit out profiling data.

```sh
root@dev:~# strace -c -p 11084
```

```sh
root@dev:~# strace -c >/dev/null ls
```

5. Why can't I connect to that server?
----------------------

strace is less chatty than tcpdump, because it returns data related to the syscalls generated by "your" process.
This is an example of a trace of "nc" connecting to www.news.com on port 80 without any problems:

```sh
$ strace -e poll,select,connect,recvfrom,sendto nc www.news.com 80
```

Java programming crash with no clear message in the stack trace to indicate the cause of the problem. In AWS, the
port was not exposed to external network access. It took lot of time and mental energy to narrow down the problem.
What tool can be used to find this type of problem fast in the future?

References:
==========
----------

1. [5 simple ways to troubleshoot using Strace] (http://www.hokstad.com/5-simple-ways-to-troubleshoot-using-strace 'strace')
2. [Finding and resolving deadlocks - TWiki - Cern] (https://twiki.cern.ch/twiki/bin/view/CMS/PhedexOperationsDeadlocks 'deadlock')
3. [strace(1) - Linux man page] (http://linux.die.net/man/1/strace 'strace')
4. [UNIX System Calls] (http://www.di.uevora.pt/~lmr/syscalls.html 'UNIX System Calls')
5. [Unix Signals] (http://www.cis.temple.edu/~ingargio/cis307/readings/signals.html 'Unix Signals')
6. [UNIX Introduction](http://www.ee.surrey.ac.uk/Teaching/Unix/unixintro.html 'UNIX Introduction')
7. UNIX Unleashed, System Administrator's Edition
