
begin
  q = Queue.new
  q.pop
rescue Exception => e
  puts "Sleeping for 10 seconds"
  sleep 10
  puts "Exception occurred due to : #{e.message}"
end

